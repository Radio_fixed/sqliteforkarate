import DBHelper.sqlite_db_operations as my_db
import DBHelper.dbddl as ddl
import microservices.tasks as myapp
if __name__ == '__main__':
	sqlite_only = False
	flask_only = False if sqlite_only else True

	if sqlite_only:
		db_my_list = my_db.DBMyList("MYList.db")
		db_my_list.create_connection()

		status, failure = db_my_list.create_table(ddl.ddl_create_task_table)

		print(f"Status:{status} Success {failure}")
		rows = db_my_list.delete_all_tasks(ddl.delete_all_tasks)
		print("Deleted rows-->", rows)

		max_id = db_my_list.create_task(ddl.insert_task, ("Buy Milk", 0))
		print(max_id)
		max_id = db_my_list.create_task(ddl.insert_task, ("Buy Bread", 0))
		print(max_id)
		max_id = db_my_list.create_task(ddl.insert_task, ("Buy oats", 0))
		print(max_id)
		db_my_list.create_task(ddl.insert_task, ("Buy quinoa", 0))
		db_my_list.update_task_title(ddl.update_task_title, ("Buy oats - not processed", 3))

		db_my_list.update_task_title(ddl.update_task_title, ("This task does not exist", 30))  # negative case

		db_my_list.update_is_done(ddl.update_is_done, (1, 3))
		db_my_list.delete_task(ddl.delete_task, (2,)) # Task 2 "Buy Bread" marked for deletion
		db_my_list.select_is_done_status(ddl.select_all_is_done_status, (1,))
		db_my_list.select_all(ddl.select_all)

	if flask_only:
		myapp.start_the_server()




